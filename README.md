
# terraform-gcp-dataproc

Manages a Cloud Dataproc cluster resource within GCP. For more information see [the official dataproc documentation](https://cloud.google.com/dataproc/).


## Prerequisites

* [ Terraform Installed](https://learn.hashicorp.com/terraform/getting-started/install.html)
* [Inspec Installed](https://www.inspec.io/downloads/)
* [Python Installed](https://www.python.org/downloads/)
* [Terraform-Docs](https://github.com/segmentio/terraform-docs)
* [Google Cloud SDK](https://cloud.google.com/sdk/install)

*Clone this Repository*

### Configure a Service Account

In order to execute this module you must have a Service Account with the following:


### Enable APIs

In order to operate with the Service Account you must activate the following APIs on the project where the Service Account was created:

- Cloud Dataproc API

#### Service Account Credentials

You can pass the service account credentials into this module by setting the following environment variables:

* `GOOGLE_CREDENTIALS`
* `GOOGLE_CLOUD_KEYFILE_JSON`
* `GCLOUD_KEYFILE_JSON`

## Testing

### Requirements

- [bundler](https://bundler.io/)
- [ruby](https://www.ruby-lang.org/)
- [python](https://www.python.org/getit/) 2.7.x
- [terraform-docs](https://github.com/segmentio/terraform-docs)
- [google-cloud-sdk](https://cloud.google.com/sdk/)

### Create credentials file via:
```bash
$ gcloud auth application-default login
```

If successful, this should be similar to:
```
$ cat ~/.config/gcloud/application_default_credentials.json
{
  "client_id": "764086051850-6qr4p6gpi6hn50asdr.apps.googleusercontent.com",
  "client_secret": "d-fasdfasdfasdfaweroi23jknrmfs;f8sh",
  "refresh_token": "1/asdfjlklwna;ldkna'dfmk-lCkju3-yQmjr20xVZonrfkE48L",
  "type": "authorized_user"
}
```

then you can setup your environment variable
```
$ export GOOGLE_APPLICATION_CREDENTIALS="~/.config/gcloud/application_default_credentials.json"
```

### run your Terraform Commands

```bash
$ terraform validate
$ terraform plan
$ terraform apply
```

you should get a successful apply and an output similar to the one below

```bash
Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

```
### DON'T forget to tear down your resources

```bash
$ terraform destroy
.......
.......
Destroy complete! Resources: 1 destroyed.

```

## GCP Permissions

Ensure the [Cloud Dataproc API](https://console.cloud.google.com/apis/library/dataproc.googleapis.com) is enabled for the current project.
