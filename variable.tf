variable "project_id" {
	description = "The ID of the project in which the dataproc will be created."
	default="tkproject-251206"
}
variable "region" {
	description = "The region of service"
	default="us-central1"
}
variable "credentials" {
	description = "The credentials of service account"
	default = "./creds/serviceaccount.json"
}

#variable "subnet_dataproc" {}
variable "instance_num_master" { default = "1"}
variable "instance_num_worker" { default = "2"}
variable "machine_type_master" { default = "n1-standard-1"}
variable "disk_size_master" { default = "15"}
variable "machine_type_worker" { default = "n1-standard-1"}
