#!/bin/sh
set -e

export TF_VAR_subnet_dataproc=$subnet_name
export TF_VAR_project_id=$project_id
export TF_VAR_region=$region
export TF_VAR_instance_num_master=$instance_num_master
export TF_VAR_instance_num_worker=$instance_num_worker
export TF_VAR_machine_type_master=$machine_type_master
export TF_VAR_machine_type_worker=$machine_type_worker
export TF_VAR_disk_size_master=$disk_size_master

gcloud auth activate-service-account --project=$TF_VAR_project_id --key-file=$TF_VAR_credentials
gcloud services enable dataproc.googleapis.com

terraform init
terraform plan -out=plan
terraform apply plan



echo "cleaning up..."
 rm -R .terraform
 rm -rf terraform.tfs*

echo "cleanup complete"
