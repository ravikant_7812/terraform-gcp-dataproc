provider "google" {
 credentials = "${var.credentials}"
 region      = "${var.region}"
 project     = "${var.project_id}"
}

module "dataproc" {
  source = "./gcp-dataproc"
  region      = "${var.region}"
  #subnet_self_link = "${var.subnet_dataproc}"
  instance_num_master = "${var.instance_num_master}"
  instance_num_worker = "${var.instance_num_worker}"
  machine_type_master = "${var.machine_type_master}"
  machine_type_worker = "${var.machine_type_worker}"
  disk_size_master = "${var.disk_size_master}"
}

#terraform {
#	backend "remote" {}
#}
