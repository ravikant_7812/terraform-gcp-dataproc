#variable "subnet_self_link" {}
variable "location" { default = "US"}
variable "staging_bucket_name" { default = "dataproc_bucket" }
variable "region" {}
variable "cluster_name" {default = "cluster"}
variable "disk_type_master" { default = "pd-ssd"}
variable "instance_num_master" { default = "1"}
variable "instance_num_worker" { default = "2"}
variable "machine_type_master" { default = "n1-standard-1"}
variable "disk_size_master" { default = "15"}
variable "no_of_instances" { default = "0"}
variable "machine_type_worker" { default = "n1-standard-1"}
variable "disk_size_worker" { default = "15"}
variable "local_ssds_worker" { default = "1"}
variable "image" { default = "1.3.7-deb9" }
variable "init_script_bucket" { default = "gs://dataproc-initialization-actions/stackdriver/stackdriver.sh" }
