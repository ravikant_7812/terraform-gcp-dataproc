resource "random_id" "id" {
 byte_length = 4
 prefix      = "dataproc-"
}

resource "google_storage_bucket" "dataproc-staging-bucket" {
  name            = "${random_id.id.hex}"
  location        = "${var.location}"
  labels = {
      pattern = "supervised-1"
  }
}

resource "google_dataproc_cluster" "devcluster" {
    name       = "${var.cluster_name}-${random_id.id.hex}"
    region     = "${var.region}"
    labels = {
        pattern = "supervised-1"
    }

    cluster_config {
        staging_bucket        = "${google_storage_bucket.dataproc-staging-bucket.name}"

        master_config {
            num_instances     = "${var.instance_num_master}"
            machine_type      =  "${var.machine_type_master}"
            disk_config {
                boot_disk_type = "${var.disk_type_master}"
                boot_disk_size_gb = "${var.disk_size_master}"
            }
        }

        worker_config {
            num_instances     = "${var.instance_num_worker}"
            machine_type      = "${var.machine_type_worker}"
            disk_config {
                boot_disk_size_gb = "${var.disk_size_worker}"
                num_local_ssds    = "${var.local_ssds_worker}"
            }
        }

        preemptible_worker_config {
            num_instances     = "${var.no_of_instances}"
        }

        # Override or set some custom properties
        software_config {
            image_version       = "${var.image}"
            override_properties = {
                "dataproc:dataproc.allow.zero.workers" = "true"
            }
        }

        

        # You can define multiple initialization_action blocks
        initialization_action {
            script      = "${var.init_script_bucket}"
            timeout_sec = 500
        }

    }
}
